Feature: Provide estimated price
    As a passenger, when I submit my request
    I will get the estimated price for this service
    
    
   Scenario: Get price
     
     Given a passenger submit his request in the origin "Liivi 2, Tartu, Estonia" and destination " Uus-Sadama 25, Tallinn, Estonia"
     When  Taxi driver accepts his request
     Then  The system provides passenger an estimated price based on the distance of trip and time hour