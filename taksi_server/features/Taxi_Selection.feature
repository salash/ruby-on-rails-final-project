Feature: multiple Taxi for selection
         As a customer
         So that I can have a taxi booking confirmed
         I want one taxi to be selected
 Background:
     Given I am at "Liivi 2, Tartu, Estonia"
       @javascript
   Scenario: Closest taxi
     Given there are two taxi available, on "Riia 1, Tartu, Estonia" and the other at "Ringtee 75, Tartu, Estonia"
     When I submit a booking request
     Then I should receive a confirmation with the taxi next to "Riia 1, Tartu, Estonia"
     And I should receive a delay estimate

   