Feature: As a taxi driver
        when I get a new order from server
        I can take it or leave it
    @javascript
Scenario: Taxi driver get a new order
  Given a passenger asks for a service in "Liivi 2, Tartu, Estonia"
  And   he submits his request in the system
  Then   Taxi driver recives a new order
  And   Taxi driver can choose to take the order for this customer
  