Given(/^a passenger asks for a service in "(.*?)"$/) do |arg1|
  
  @customer = Capybara::Session.new(:poltergeist)
  @customer.visit "http://agile-system-lashkarara.c9users.io:8080/#/bookings"
  @customer_address=arg1
  @customer.fill_in "Customer_Address_ID", with: arg1
  @customer.fill_in "Customer_Destination_ID", with: " Uus-Sadama 25, Tallinn, Estonia"
 
  
end

Given(/^he submits his request in the system$/) do
     @customer.click_button "submit"

end

Then(/^Taxi driver recives a new order$/) do

    @taxiDriver = Capybara::Session.new(:poltergeist)
    @taxiDriver.visit "http://agile-system-lashkarara.c9users.io:8082/#/GetService"
    @taxiDriver.page.has_content?(@customer_address)    
end

Then(/^Taxi driver can choose to take the order for this customer$/) do
      @taxiDriver.choose "Take_or_Leave_0"
      @taxiDriver.click_button "submit"
end


