Given(/^taxi driver "(.*?)" has an account$/) do |arg1|
  @User_name=arg1
  @Password=arg1
end

When(/^taxi driver loggs in to the system$/) do

   visit "http://agile-system-lashkarara.c9users.io:8082/#/Login"
   fill_in "Username", with: @User_name
   fill_in "Password", with: @Password
   click_button "submit"

end

Then(/^He must see "(.*?)"$/) do |arg1|
  page.has_content?("Hello #{@User_name}")
end
