Given(/^there are two taxi available, at the same distance with respect to "(.*?)"$/) do |arg1|
   Taxi.create(:taxiNo => "T103", :address => arg1, :status =>"1" , :avalible_duration => 2   )
   Taxi.create(:taxiNo => "T104", :address => arg1, :status =>"1" , :avalible_duration => 6   )
end

When(/^I submit a booking request$/) do
  
  visit "http://agile-system-lashkarara.c9users.io:8080/#/bookings"
  fill_in "Customer_Address_ID" , with:"Liivi 2, Tartu, Estonia"
  fill_in "Customer_Address_ID" , with:" Uus-Sadama 25, Tallinn, Estonia"
  click_button "submit"
  
end

Then(/^I should receive a confirmation with the taxi that has been available the longest$/) do
    page.has_content?("We have already sent taxi T104")
end

Then(/^I should receive a delay estimate$/) do
  page.has_content?(" Your taxi will arrive in ")
end