
Given(/^I am at "(.*?)"$/) do |taxi_Address|
 
    @taxi_address=taxi_Address
 
end

Given(/^I am "(.*?)" for next possible service$/) do |taxi_stutus|
  @status=taxi_stutus
  
end

Then(/^I submit my status in the system$/) do
 # @taxiDriver = Capybara::Session.new(:poltergeist) 
 # @taxiDriver.visit "http://agile-system-lashkarara.c9users.io:8082/#/Taxi_Status"
 # @taxiDriver.click_button "submit"
  
  visit "http://agile-system-lashkarara.c9users.io:8082/#/Taxi_Status"
  fill_in "Address", with: @taxi_address
  fill_in "Taxi_No", with: "T1"
  click_button "submit"
  
end

Then(/^The system replies me$/) do
  
  # @taxiDriver.page.has_content?('This service is reserved for you.')
   page.has_content?('Your status get updated')
  
end