Given(/^a passenger submit his request in the origin "(.*?)" and destination "(.*?)"$/) do |origin, destination|
  @customer = Capybara::Session.new(:poltergeist)
  @customer.visit "http://agile-system-lashkarara.c9users.io:8080/#/bookings"
  @customer.fill_in "Customer_Address_ID", with: origin
  @customer.fill_in "Customer_Destination_ID", with: destination
  @customer.click_button "submit"
end

When(/^Taxi driver accepts his request$/) do

    @taxiDriver = Capybara::Session.new(:poltergeist)
    @taxiDriver.visit "http://agile-system-lashkarara.c9users.io:8082/#/GetService"
    @taxiDriver.choose "Take_or_Leave_0"
    @taxiDriver.click_button "submit"

end

Then(/^The system provides passenger an estimated price based on the distance of trip and time hour$/) do
  @customer.page.has_content("The average costs for at ")
end