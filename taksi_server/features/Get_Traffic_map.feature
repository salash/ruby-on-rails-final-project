Feature: Get Traffic information 
       As a taxi driver when i get a new 
       order, a map appears in my page informing me 
       the traffic situation around the passenger address
 Scenario: Getting traffic information   
    Given There is a passenger in "Liivi 2, Tartu, Estonia"   
    When   He submits his request in the system
    Then  A map appears in the my page
    And   it shows traffic information arround passenger