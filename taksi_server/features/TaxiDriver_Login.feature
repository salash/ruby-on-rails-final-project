Feature: Logging into the system
    As a taxi driver, he can insert his
    username and password and logs in to his
    account
    
    @javascript
    Scenario: Logging to the system
       Given taxi driver "T1" has an account
       When taxi driver loggs in to the system
       Then He must see "Wellcome to your account T1"