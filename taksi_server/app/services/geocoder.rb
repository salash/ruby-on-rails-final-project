require 'uri'
require 'net/http'
require 'active_support'
require 'active_support/core_ext'
require 'byebug'

class Geocoder
   def self.firstEstimation(origin,destination)
        org=URI.escape(origin)
        des=URI.escape(destination)
        uri = URI("https://maps.googleapis.com/maps/api/directions/json?origin=#{org}&destination=#{des}&key")
        response= Net::HTTP.get(uri)
        json=ActiveSupport::JSON.decode(response)
       # puts json
        value=json["routes"][0]["legs"][0]["duration"]["value"]
        res=value/60
      #  byebug
        res 
   
   
   
   end
    def self.locate(address)
        str=URI.escape(address)
        

    #    uri = URI("http://maps.googleapis.com/maps/api/geocode/json?address=#{str}&sensor=false")
        uri = URI("https://maps.googleapis.com/maps/api/geocode/json?address=#{str}&key=AIzaSyC-FkZYHwApDZMB5RjcRIkiNegKq0edeks")
        response= Net::HTTP.get(uri)
        json=ActiveSupport::JSON.decode(response)
      #  puts json
        lat=json["results"][0]["geometry"]["location"]["lat"]
        lng=json["results"][0]["geometry"]["location"]["lng"]
        
        {:lat => lat, :lng => lng} 
    end

   def self.computeDelay(customer_address, taxi_address)

     #  taxi_address="Riia 1, 51013 Tartu"  
       taxi_address_str = URI.escape(taxi_address)
       customer_address_str = URI.escape(customer_address)
       
       link="https://maps.googleapis.com/maps/api/distancematrix/json?origins=#{taxi_address_str}&destinations=#{customer_address_str}&mode=driving&key=AIzaSyC-FkZYHwApDZMB5RjcRIkiNegKq0edeks"
      
       uri = URI.parse(link)    
       response = Net::HTTP.get(uri)

       json = ActiveSupport::JSON.decode(response)
    #   puts json
       
       if json.nil?
           puts "json is nil"
       else
        duration = json["rows"][0]["elements"][0]["duration"]["text"]
        distance = json["rows"][0]["elements"][0]["distance"]["text"]
       end
      
     #  byebug
       hours = duration.match(/[\d]* hour/).to_s.gsub(/[^\d]/, '')
       minutes = duration.match(/[\d]* mins/).to_s.gsub(/[^\d]/, '')
       distance_in_km = distance.match(/[\d]* km/).to_s.gsub(/[^\d]/, '')
      # byebug
       
       if hours==""
          result={:hours => 0 , :minutes => minutes}
       elsif minutes==""
          result={:hours => hours , :minutes => 0}          
       else
          result={:hours => hours, :minutes => minutes}
        
       end
       distance_in_km=1 if distance_in_km==""     #In case the distance between taxi and passenger is very short, we consider it as 1 km
           
       dur=(result[:hours].to_i) *60+(result[:minutes].to_i)
      
       dur=3 if dur==0                           #In case the duration between taxi and passenger is very short, we consider it as 3 min
        

       {:duration =>dur, :distance =>distance_in_km}
       
   end
   
end
