require 'time'

class BookingsController < ApplicationController
  def create

     if (params[:Cancelation]==1)
         Pusher.trigger('NewOrder', 'async_notification', {  message: "Stop. The passenger has canceled his request. Please update your status for the system", get_Driver_Location: 1, to: params[:taxi_No] })        
         render :text => {:message => "Your order has been canceled"}.to_json, :status => :created
     else
        
         rejecting_Taxi_number=0
         Passenger.create(:name => "Salman", :address =>params[:Address], :destination  => params[:Destination])
         get_recent_passenger_id=Passenger.all.order(created_at: :desc).first.id
         coordination=Geocoder.locate(params[:Address])
            
         selected_Taxi_list=select_Best_Taxis(params[:Address])
         selected_Taxi=selected_Taxi_list[0]
         taxi_no=selected_Taxi[:taxi_No]
         #byebug
         
         time_needed=Geocoder.firstEstimation(params[:Address],params[:Destination])
         cost=compute_Taxi_Fare(time_needed) 
         render :text => {:message => "Booking is being processed, possible cost is #{cost}", :taxi_No => taxi_no }.to_json, :status => :created
         
         #   byebug
         Pusher.trigger('NewOrder', 'async_notification', {  message: params[:Address] , passenger_id: get_recent_passenger_id, lat:  coordination[:lat], long:  coordination[:lng], to: taxi_no , nunmbrtOfTaxiRejected: rejecting_Taxi_number })        
     
     end
  
  end
  
  def responce
    
                if(params[:Take_New_Service_Status]=="1")
            
                   passenger_address=Passenger.find_by(id: params[:passenger_id]).address
                   passenger_destination=Passenger.find_by(id: params[:passenger_id]).destination
                   delay_to_reach_destination=Geocoder.computeDelay(passenger_address, passenger_destination) 
            #       cost_for_service=((delay_to_reach_destination[:distance]).to_i) *3
                   delay=Geocoder.computeDelay(passenger_address, passenger_destination)
                   selected_Taxi_list=select_Best_Taxis(passenger_address)
                   selected_Taxi=selected_Taxi_list[0]
                   taxi_no=selected_Taxi[:taxi_No]
                   cost=compute_cost(passenger_address,passenger_destination)
                                 
            #         Order.create!(passenger_id: 1,taxi_id:2,price: cost)    
               #     byebug
                   
                   t = Time.now
                   time=t.strftime("at %I:%M%p") 
                   cost_rate=getrate()
#=begin           
               #   byebug
                  creat_Invoice(params[:passenger_id],selected_Taxi[:taxi_id],cost)      
                  Pusher.trigger('bookings', 'async_notification', {  message: "We have already sent taxi #{taxi_no}"+"\n "+ 
                                                                               "Your taxi will arrive in #{delay[:duration]} minutes"+" \r\n "+
                                                                               "The distance is #{delay[:distance]}"+"\r\n"+
                                                                               "The average costs for #{time} is "+"\r\n"+
                                                                               "#{cost_rate} euros per kilometer"+"\r\n"+
                                                                               "The totall cost for you is #{(cost) }"
                  })
                  
#=end                  
                   render :text => {:message => "#{taxi_no} This service is reserved for you"}.to_json, :status => :created 
                
                  
                   
                else
                    
                   render :text => {:message => "Thanks for your responce. Please remain avalible for other orders"}.to_json, :status => :created    
                   #byebug    
                   if(params[:nunmbrtOfTaxiRejected].to_i > 6)
                           rejecting_Taxi_number=(params[:nunmbrtOfTaxiRejected].to_i) + 1
                           pass =Passenger.find_by_id(params[:passenger_id])
                           passenger_address=pass.address
                           get_recent_passenger_id=pass.id
                           coordination=Geocoder.locate(passenger_address)
                           
                           selected_Taxi_list=select_Best_Taxis(passenger_address)
                           selected_Taxi=selected_Taxi_list[rejecting_Taxi_number]
                           taxi_no=selected_Taxi[:taxi_No]
                         #  byebug
                           Pusher.trigger('NewOrder', 'async_notification', {  message: params[:Address] , passenger_id: get_recent_passenger_id, lat:  coordination[:lat], long:  coordination[:lng], to: taxi_no, nunmbrtOfTaxiRejected: rejecting_Taxi_number })        
                   else
                      Pusher.trigger('bookings', 'async_notification', {  message:"Sorry we have failed to find a service for u." })     
                   end
                end
  end
  def compute_cost(origin,destination) 
      delay=Geocoder.computeDelay(origin, destination) 
       cost_rate=getrate()
      cost_for_delivery=((delay[:distance]).to_i) * cost_rate
      
      cost_for_delivery
  end
  def getrate
      t=Time.now
       hour=t.hour
    #  byebug
      if 0<hour && hour<6
           cost_rate=5
      else
            cost_rate=3
      end
      
      cost_rate
  end
  def creat_Invoice(passenger_ID,taxi_ID,price_amount)
    invoice=Order.create!(passenger_id: passenger_ID,taxi_id: taxi_ID, price: price_amount) 
    if invoice.valid?
        true
    else
        false
    end
      
  end
  def update_status
      
        if(Taxi.find_by_taxiNo(params[:TaxiNo])!=nil )
             render :text => {:message => "Your status get updated"}.to_json, :status => :created      
        else
        
          Taxi.create(:taxiNo => params[:TaxiNo], :address => params[:Address], :status =>params[:Status] , :avalible_duration => 0   )
          render :text => {:message => "A new Taxi created"}.to_json, :status => :created           
          
        end  
  end
  
  def select_Best_Taxis(customer_address)
  
    avalibe_taxi=Taxi.where(status: "1")
     
    list=Array.new
  
     avalibe_taxi.find_each  do |taxi|
    #    byebug
        delay= Geocoder.computeDelay(customer_address, taxi.address)
              
         res={:taxi_No => taxi.taxiNo, :taxi_Delay => delay[:duration], :distance => delay[:distance] , :address => taxi.address, :avalible_duration => taxi.avalible_duration, :taxi_id => taxi.id}      

        # puts res

        list.push(res)
     end
     sorted_List_of_Taxis = list.sort_by { |k| k[:taxi_Delay] }
    
     puts "Selected taxi is #{sorted_List_of_Taxis[0]}"
    # byebug
     best_Delay=sorted_List_of_Taxis[0][:taxi_Delay]        
   # byebug 
    selected_taxis=sort_based_on_avalibilty(sorted_List_of_Taxis,best_Delay)
    selected_taxis
    
    # byebug    
  end
##############################################################################################33  
  def sort_based_on_avalibilty(list_of_Candidated_Taxis,best_Delay)
  
     final_Taxi=Array.new
     list_of_Candidated_Taxis.each do |taxi| 
         
         if(taxi[:taxi_Delay]==best_Delay )
              final_Taxi.push(taxi)        
         end
     end
     sorted_List_of_Taxis = final_Taxi.sort_by { |k| k[:avalible_duration] } 
     sorted_List_of_Taxis=sorted_List_of_Taxis.reverse
    # sorted_List_of_Taxis[0]
    # byebug
  end
  def compute_Taxi_Fare(time_needed) 
            
            t=Time.now
            hour=t.hour
            week_day=t.strftime("%A")
           if ((week_day== "Friday" || week_day== "Saturday") && (hour==22 ||hour==23 || hour<5))
              flagfall=6.5                         
              fare=0.915
           elsif(9<hour && hour<18)
              flagfall=4.2
              fare=0.568
          # elsif(18<hour && hour<9)
          else
              flagfall=5.2
              fare=0.631  
              
           end
         #  byebug
           res= flagfall+time_needed * fare
           res
    #       byebug
  end
  
end
