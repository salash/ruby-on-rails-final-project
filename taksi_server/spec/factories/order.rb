require 'faker'

FactoryGirl.define do
  factory :order do
    passenger_id 1
    taxi_id  2
    created_at Time.now
    updated_at Time.now + 1
  end
end