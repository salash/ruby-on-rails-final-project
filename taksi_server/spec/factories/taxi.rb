require 'faker'

FactoryGirl.define do
  factory :taxi do
    taxiNo "T1"
    address  "Liivi 2, Tartu, Estonia"
    status "1"
    avalible_duration 2
    created_at Time.now
    updated_at Time.now + 1
  end
end