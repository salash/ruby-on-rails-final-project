require 'faker'

FactoryGirl.define do
  factory :passenger do
    name "Salman"
    address  "Liivi 2, Tartu, Estonia"
    destination "Ringtee 75, Tartu, Estonia"
    created_at Time.now
    updated_at Time.now + 1
  end
end