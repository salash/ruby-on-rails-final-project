require 'rails_helper'
require 'spec_helper'
require 'bookings_controller'

RSpec.describe BookingsController, type: :controller do
    
   context 'Select best Taxi based on delay' do
       
       it "should select nearest taxi to the passenger" do
          Taxi.create!(taxiNo: "T100",address: "Uus-Sadama 25, Tallinn, Estonia",status:1,avalible_duration: 6)
          Taxi.create!(taxiNo: "T101",address: "Narva mnt25, Tartu, Estonia"    ,status:1,avalible_duration: 2)
          choose=BookingsController.new()
          
          selected_Taxi= choose.select_Best_Taxis(' Uus-Sadama 25, Tallinn, Estonia')
       
          taxi_no=selected_Taxi[:taxi_No] 
           expect(taxi_no).to eq("T100")
       
       end
       
       it "should choose the taxi which has been more avalible" do
          Taxi.create!(taxiNo: "T102",address: "Uus-Sadama 25, Tallinn, Estonia",status:1,avalible_duration: 6)
          Taxi.create!(taxiNo: "T103",address: "Uus-Sadama 25, Tallinn, Estonia",status:1,avalible_duration: 2)
          choose=BookingsController.new()
          
          selected_Taxi_list= choose.select_Best_Taxis(' Uus-Sadama 25, Tallinn, Estonia')
       
          taxi_no=selected_Taxi[:taxi_No] 
           expect(taxi_no).to eq("T102")
           
           
       end
       it "should compute costs" do
          Taxi.create!(taxiNo: "T101",address: "Narva mnt25, Tartu, Estonia"    ,status:1,avalible_duration: 2)
          sample_booking=BookingsController.new()
          cost=sample_booking.compute_cost('Liivi 2, Tartu, Estonia','Uus-Sadama 25, Tallinn, Estonia') 
         
           expect(cost).to eq(555)
           
           
       end
    
    
    
    
   end
#
=begin
   context "It should make invoices" do


               it "should return a table for set of invoices" do

                  Passenger.create!(:name => "Salman", :address =>"Liivi 2, Tartu, Estonia", :destination  => " Uus-Sadama 25, Tallinn, Estonia")

                  get_recent_passenger_id=Passenger.all.order(created_at: :desc).first.id
                  
                  Taxi.create!(taxiNo: "T2",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
                  get_recent_Taxi_id=Taxi.all.order(created_at: :desc).first.id
                
                  sample_booking=BookingsController.new()                
                  cost=sample_booking.compute_cost("Liivi 2, Tartu, Estonia","Uus-Sadama 25, Tallinn, Estonia")
                   
                 reult=sample_booking.creat_Invoice(get_recent_passenger_id,get_recent_Taxi_id,cost)               
                  
              
                  
    #              taxi_driver=TaxidriverController.new();
    #              invoice=taxi_driver.getInvoices("T2")
                                      
     #             expect(invoice).to eq([{ passenger: "Salman", Origin:"Liivi 2, Tartu, Estonia",
      #                         Destination:" Uus-Sadama 25, Tallinn, Estonia", Price:"1000"}]) 
       
                 expect(reult).to eq(true)
       
               end

   end
=end   
#=begin   
   context "STRS system" do
     it "should compute taxi fare" do
         t = Time.local(2016, 1, 2, 12, 5, 0)
         Timecop.freeze(t)
         cost= BookingsController.new.compute_Taxi_Fare(100)
         expect(cost).to eq(61)
     end
   end
#=end

end