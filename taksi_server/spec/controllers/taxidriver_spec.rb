require 'rails_helper'
require 'spec_helper'
require 'taxidriver_controller'

RSpec.describe TaxidriverController, type: :controller do
#let(:taxi_driver) { TaxiDriver.new }
    context 'POST /Login' do


        it 'returns valid information when usernamr=password ' do
            Taxi.create!(taxiNo: "T1",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
            
            Taxi.create!(taxiNo: "T2",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
            post :login, Username: "T1", Password: "T1"
    
            expect(response.message).to eq("Created")
        end
    
        it 'Returns nothing incase of not matching username and password' do
            Taxi.create!(taxiNo: "T1",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
            
            Taxi.create!(taxiNo: "T2",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
            
           post :login, Username: "T1", Password: "wrong"
         #  byebug
           expect(response.status).to eq(401) 
        end
    end 
    context 'Get /Invoices' do
               it "should load data of invoices" do
                  post :getInvoices , taxiNo: "T2"
                        
                  expect(response.status).to eq(201) 
               end
               it "should return list of invoices for a certain taxi driver" do
               
                  Passenger.create!(:name => "Salman", :address =>"Liivi 2, Tartu, Estonia", :destination  => " Uus-Sadama 25, Tallinn, Estonia")

                  get_recent_passenger_id=Passenger.all.order(created_at: :desc).first.id
                  
                  Taxi.create!(taxiNo: "T2",address: "Liivi 2, Tartu, Estonia",
                               status:1,avalible_duration: 6)
                  get_recent_Taxi_id=Taxi.all.order(created_at: :desc).first.id
                
                   
                 
                  o=Order.create!(passenger_id: get_recent_passenger_id,
                                taxi_id: get_recent_Taxi_id, price: 1000) 
               #   byebug
#=end
                  taxi_driver=TaxidriverController.new();
                  
                #  invoice=taxi_driver.getInvoices("T2")
                  
                                      
                  res=[{ passenger: "Salman", Origin:"Liivi 2, Tartu, Estonia",Destination:" Uus-Sadama 25, Tallinn, Estonia", Price:"1000"}]
                  expect(taxi_driver.load_Invoices("T1")).to eq(res) 
       
               end
    end       
end