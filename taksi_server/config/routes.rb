Rails.application.routes.draw do
  match '*all' => 'application#cors_preflight', via: :options

  post "/Invoices",   :to => "taxidriver#getInvoices"
         
  post "/Login",      :to => "taxidriver#login"
  
  post "/bookings", :to => "bookings#create"
  
#  post "/Taxi_Status",     :to => "taxidriver#create"

  post "/Taxi_Status",     :to => "bookings#update_status"
#  post "/GetService",      :to => "taxidriver#create"
  post "/GetService",      :to => "bookings#responce"
#  post "/GetService",      :to => "bookings#create"
  
end
