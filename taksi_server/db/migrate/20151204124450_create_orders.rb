class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :passenger, index: true
      t.belongs_to :taxi, index: true
      t.integer :price
      t.timestamps null: false
    end
  end
end
