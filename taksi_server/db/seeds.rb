# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
more_Taxi = [
  {:taxiNo => 'T1', :address => 'Ringtee 75, Tartu, Estonia', :status => '1', :avalible_duration => 1 },
  {:taxiNo => 'T2', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  
  {:taxiNo => 'T110', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  {:taxiNo => 'T111', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  {:taxiNo => 'T112', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  {:taxiNo => 'T113', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  {:taxiNo => 'T114', :address => 'Liivi 2, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
  
  
  
  
  
  {:taxiNo => 'T3', :address => 'Ujula 2, Tartu, Estonia', :status => '1', :avalible_duration => 3 },
  {:taxiNo => 'T4', :address => 'Sõbra 56, Tartu, Estonia', :status => '1', :avalible_duration => 4 },
  {:taxiNo => 'T5', :address => 'Narva mnt25, Tartu, Estonia', :status => '1', :avalible_duration => 5 },
  {:taxiNo => 'T6', :address => 'Narva mnt25, Tartu, Estonia', :status => '1', :avalible_duration => 6 },
   
  {:taxiNo => 'T7', :address => 'Uus-Sadama 25, Tallinn, Estonia', :status => '1', :avalible_duration => 2 },
  {:taxiNo => 'T8', :address => 'Riia 1, Tartu, Estonia', :status => '0', :avalible_duration => 2 },
 
 
  {:taxiNo => 'T9', :address => 'Riia 1, Tartu, Estonia', :status => '1', :avalible_duration => 2 },
 
]

more_Taxi.each do |t|
  Taxi.create!(t)
end