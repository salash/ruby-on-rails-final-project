Hi, when you download the code and trying to run your own frontend and backend, in order to make **pusher communication** work  you must: 

**1.** in frontend_client dist/scripts/services/bookings_service 
  
  change the destination you are trying to reach to your backend address: 
   http://yourworkspace-yournam.c9users.io:8081/bookings'

**2.** in frontend_taxidriver dist/js/service/get_service.js
   http://yourworkspace-yournam.c9users.io:8081/GetService'

**3.** in frontend_taxidriver dist/js/service/taxi_status_service.js
   http://yourworkspace-yournam.c9users.io:8081/Taxi_Status'

**All frontends must be reaching YOUR server**


**When trying to run cucumber tests:**

1. make sure you are running your frontend
2. in your cucumber steps make sure you are actually reaching your frontend    address

**Correct the installation of Ionic
**
sudo npm cache clean -f
sudo npm install -g n
sudo n stable


hook file fix
chmod -R 755 hooks/