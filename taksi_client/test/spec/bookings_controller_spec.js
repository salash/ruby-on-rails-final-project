'use strict';

describe('BookingsCtrl', function () {

  beforeEach(module('taksi_client'));

  var BookingsCtrl,
    scope,
    $httpBackend;

  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_, _BookingsService_, _PusherService_) {
    scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;

    BookingsCtrl = $controller('BookingsCtrl', {
      $scope: scope,
      BookingsService: _BookingsService_,
      PusherService: _PusherService_
    });
  }));

  it('should submit a request to the backend service', function () {
    $httpBackend
      .expectPOST('http://taxi-lashkarara.c9users.io:8081/bookings',
            {Address: " Uus-Sadama 25, Tallinn, Estonia", Destination: "Narva mnt25, Tartu, Estonia"})
      .respond(201, {message: 'Booking is being processed'});

    scope.Customer_Address = " Uus-Sadama 25, Tallinn, Estonia";
    scope.Customer_Destination = "Narva mnt25, Tartu, Estonia";
    scope.submit();
    $httpBackend.flush();

    var stub = Pusher.instances[0];
    var channel = stub.channel('bookings');
    channel.emit('async_notification', {message: 'Your taxi will arrive in 3 minutes'})

    expect(scope.syncNotification).toBe('Booking is being processed');
    expect(scope.asyncNotification).toBe('Your taxi will arrive in 3 minutes');
  });
  
   it('should recieve an estimation', function () {
    $httpBackend
      .expectPOST('http://taxi-lashkarara.c9users.io:8081/bookings',
            {Address: " Uus-Sadama 25, Tallinn, Estonia", Destination: "Narva mnt25, Tartu, Estonia"})
      .respond(201, {message: 'Booking is being processed'});

    scope.Customer_Address = " Uus-Sadama 25, Tallinn, Estonia";
    scope.Customer_Destination = "Narva mnt25, Tartu, Estonia";
    scope.submit();
    $httpBackend.flush();

    var stub = Pusher.instances[0];
    var channel = stub.channel('bookings');
    channel.emit('async_notification', {message:"Taxi fare is about 0.635 per minute"})

    expect(scope.syncNotification).toBe('Booking is being processed');
    expect(scope.asyncNotification).toBeDefined();
  });
  
})