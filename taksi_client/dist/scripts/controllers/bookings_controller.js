'use strict'

var app = angular.module('taksi_client');

app.controller('BookingsCtrl', function ($scope, BookingsService, PusherService) {
  $scope.latitude = 0;
  
  $scope.syncNotification = '';
  $scope.submit = function() {
    BookingsService.save({Address: $scope.Customer_Address , Destination: $scope.Customer_Destination }, function (response) {
      $scope.syncNotification = response.message;
       sessionStorage.taxiNo=response.taxi_No 
    });
  };
  
  
  $scope.cancel = function() {
    BookingsService.save({Cancelation: 1,taxi_No: sessionStorage.taxiNo}, function (response) {
      $scope.syncNotification = response.message;
    });
  };
  
  
  $scope.asyncNotification = '';
  PusherService.onMessage(function(response) {
      $scope.asyncNotification = response.message;
  });
  
});


