'use strict';

var app = angular.module('taksi_client');

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('7e9ec03242b1dcea8301');
  var channel = pusher.subscribe('bookings');
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});