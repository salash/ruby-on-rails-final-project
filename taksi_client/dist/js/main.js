var app = angular.module('taksi_client', ['ngRoute','ngResource']);

app.config(function($routeProvider){
    $routeProvider
         .when('/bookings', {
          templateUrl: 'views/new.html',
          controller: 'BookingsCtrl'
        })
        .otherwise({
            redirectTo: '/bookings'
        });
});