var messages = ["Let me out!", "I'm a computer genie!", "Where am I!", "I will eat your soul!", "I wan't to see the real world!", "Why did you put me here?", "I am in"];
function getMessage() {
   return messages[Math.floor(Math.random() * messages.length)];
}


var app = angular.module('taksi_Taxi_driver_client');

app.factory('Framework', function ($q) {
  var _navigator = $q.defer();
  var _cordova = $q.defer();
  console.log(getMessage());

  if (window.cordova === undefined) {
    _navigator.resolve(window.navigator);
    _cordova.resolve(false);
  } else {
    document.addEventListener('deviceready', function (evt) {
      _navigator.resolve(navigator);
      _cordova.resolve(true);
    });
  }

  console.log(getMessage());

  return {
    navigator: function() { return _navigator.promise; },
    cordova: function() { return _cordova.promise; }
  };
});
