var app = angular.module('taksi_Taxi_driver_client');

app.controller('Taxi_StatusCtrl', function ( $scope, Taxi_Driver_Status_Service,PusherService){
    $scope.syncNotification = '';      
    $scope.submit=function(){

        Taxi_Driver_Status_Service.save({ Address: $scope.Address_Taxi_Driver, Status: $scope.Status.name, TaxiNo: $scope.TaxiNo }, function(  response  ){
            
           $scope.syncNotification = response.message;
        });
    };
    
      $scope.asyncNotification = '';
      PusherService.onMessage(function(response) {
      $scope.asyncNotification = response.message;
  });
});