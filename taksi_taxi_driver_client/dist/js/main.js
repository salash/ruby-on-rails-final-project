var app = angular.module('taksi_Taxi_driver_client', ['ngRoute', 'ngResource']);
                          
app.config(function($routeProvider){
    $routeProvider
    
    .when('/Invoices', {
         templateUrl: 'views/Taxi/Taxi_invoices.html',
         controller: 'Taxi_Get_Order'
        })
    
    .when('/Login', {
         templateUrl: 'views/Taxi/Login.html',
         controller: 'loginCtrl'
        })
    
    .when('/Taxi_Status', {
         templateUrl: 'views/Taxi/Taxi_Status.html',
         controller: 'Taxi_StatusCtrl'
        })
        
    .when('/GetService', {
         templateUrl: 'views/Taxi/GetService.html',
         controller: 'Taxi_Get_Order'
        })
        .otherwise({
            redirectTo: '/Login'
        });
});
