'use strict';

describe('loginCtrl', function () {

  beforeEach(module('taksi_Taxi_driver_client'));

  var loginCtrl,
    scope,
    $httpBackend;

  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_, _loginService_, _PusherService_) {
    scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;

    loginCtrl = $controller('loginCtrl', {
      $scope: scope,
      loginService: _loginService_,
 
    });
  }));

  it('Login should save username in case username or password is true', function () {
    $httpBackend
      .expectPOST('http://agile-system-lashkarara.c9users.io:8081/Login',
            {Username: "T1", Password: "T1"})
      .respond(201, {message: 'T1'});

    scope.username = "T1";
    scope.password = "T1";
    scope.submit();
    $httpBackend.flush();

    expect(sessionStorage.getItem('TaxiNo')).toBe('T1');
   
  });
  
  it('should show warning with wrong username and password', function () {
    
    $httpBackend
      .expectPOST('http://agile-system-lashkarara.c9users.io:8081/Login',
            {Username: "T1", Password: "wrong"})
      .respond(201, {});

    scope.username = "T1";
    scope.password = "wrong";
    scope.submit();
    $httpBackend.flush();
   // console.log("inside test  response.message "+sessionStorage.getItem('TaxiNo'))
    expect(scope.syncNotification).toBe('Username or password is wrong');
   
  });
  
})