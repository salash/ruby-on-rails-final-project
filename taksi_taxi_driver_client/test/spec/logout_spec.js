
'use strict';

describe('Taxi_Get_Order', function () {
    
      beforeEach(module('taksi_Taxi_driver_client'));
    
      var Taxi_Get_Order,
        scope,
        $httpBackend;
    
      beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
        scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
    
        Taxi_Get_Order = $controller('Taxi_Get_Order', {
          $scope: scope,
      
    });
  }));
  
  
    it ("should delete the sessions in logout",function(){
             scope.logout();
             expect(sessionStorage.getItem('TaxiNo')).toBe(null);  
      });
})  
  
  
//*/