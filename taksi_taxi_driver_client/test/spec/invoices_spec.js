'use strict';

describe('Taxi_Get_Order', function () {
          beforeEach(module('taksi_Taxi_driver_client'));
    
      var Taxi_Get_Order,
        scope,
        $httpBackend;
    
      beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
        scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
    
        Taxi_Get_Order = $controller('Taxi_Get_Order', {
          $scope: scope,
      
            });
        }));  
        it ("should load the table of invoices",function(){
               sessionStorage.setItem('TaxiNo',"T2")
                $httpBackend
                      .expectPOST('http://agile-system-lashkarara.c9users.io:8081/Invoices',
                                {TaxiNo: "T2"})
                      .respond(201, [{ passenger: "Salman", Origin:"Liivi 2, Tartu, Estonia",
                                       Destination:" Uus-Sadama 25, Tallinn, Estonia", Price:"1000"}]);
                scope.invoices();
                $httpBackend.flush();
        
                expect(scope.syncNotification).toBe( [{ passenger: "Salman", Origin:"Liivi 2, Tartu, Estonia",
                                       Destination:" Uus-Sadama 25, Tallinn, Estonia", Price:"1000"}]); 
        });
    })